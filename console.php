<?php

require_once 'vendor/autoload.php';

$handler = new \Boorwey\Console\Handler();
$handler->execute($argv);
