<?php

declare(strict_types=1);

namespace Boorwey\Console;

use Boorwey\Console\Cli\Parse;
use Boorwey\Console\Cli\Output;
use Boorwey\Console\Command\Map;

final class Handler
{
    public function execute(array $argv): void
    {
        $data = new Parse($argv);
        $errors = $data->getErrors();
        $output = new Output();

        if (!empty($errors)) {
            foreach ($errors as $error) {
                $output->msgFail($error);
            }

            return;
        }

        $map = new Map();
        $command = $data->getCommand();

        if ($command === null) {
            foreach ($map->getItems() as $k => $item) {
                $output->msgInfo($k . '(' . $item::getDescription() . ')');
            }

            return;
        }

        /** @var CommandAbstract $commandClass */
        $commandClass = $map->getItems()[$command];

        if (
            $command !== null
            && $data->isHelpParamExists()
        ) {
            $commandClass::forHelp();
            return;
        }

        $arguments = $data->getArguments();
        $parameters = $data->getParameters();

        /** @var CommandAbstract $commandObject */
        $commandObject = new $commandClass($arguments, $parameters);
        $errors = $commandObject->getErrors();

        if ($errors) {
            foreach ($errors as $error) {
                $output->msgFail($error);
            }
        } else {
            $commandObject->execute();
        }
    }
}