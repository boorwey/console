<?php

declare(strict_types=1);

namespace Boorwey\Console\Cli;

use Boorwey\Console\Validation\Argument;
use Boorwey\Console\Validation\Parameter;

final class Parse
{
    private array $argv;

    private ?string $command;

    private array $arguments;

    private array $parameters;

    private bool $isHelpParamExists;

    private array $errors;

    public function __construct(array $argv)
    {
        $this->argv = $argv;
        $this->execute();
    }

    public function getCommand(): ?string
    {
        return $this->command;
    }

    public function getArguments(): array
    {
        return $this->arguments;
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function isHelpParamExists(): bool
    {
        return $this->isHelpParamExists;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    private function execute(): void
    {
        $command = null;
        $arguments = [];
        $parameters = [];
        $isHelpParamExists = false;

        $errors = [];

        foreach ($this->argv as $k => $arg) {
            if ($k === 0) {
                continue;
            }

            if ($k === 1) {
                $command = $arg;
                continue;
            }

            if ($arg === '[help]') {
                $isHelpParamExists = true;
                continue;
            }

            $validationArg = new Argument($arg);
            $validationParam = new Parameter($arg);

            if (
                $validationArg->isError()
                && $validationParam->isError()
            ) {
                $errors[] = "Not valid input {$arg}";
            }

            if (
                !$validationArg->isError()
            ) {
                foreach ($this->getArgumentsByArg($arg) as $argument) {
                    $arguments[] = $argument;
                }
            }

            if (
                !$validationParam->isError()
            ) {
                foreach ($this->getParameterByArg($arg) as $kP => $parameter) {
                    $parameters[$kP] = $parameter;
                }
            }
        }

        $this->command = $command;
        $this->arguments = $arguments;
        $this->parameters = $parameters;
        $this->errors = $errors;
        $this->isHelpParamExists = $isHelpParamExists;
    }

    private function getArgumentsByArg(string $arg): array
    {
        $arg = str_replace(['{', '}'], '', $arg);
        return explode(',', $arg);
    }

    private function getParameterByArg(string $arg): array
    {
        $arg = str_replace(['[', ']'], '', $arg);
        $keyVal = explode('=', $arg);

        $value = $keyVal[1];
        $validationArg = new Argument($value);

        if (!$validationArg->isError()) {
            $outputValue = $this->getArgumentsByArg($value);
        } else {
            $outputValue = $value;
        }

        return [
            $keyVal[0] => $outputValue
        ];
    }
}