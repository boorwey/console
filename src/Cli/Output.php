<?php

declare(strict_types=1);

namespace Boorwey\Console\Cli;

final class Output
{
    public function msgSuccess(string $message): void
    {
        echo "{$message}\n";
    }

    public function msgFail(string $message): void
    {
        echo "{$message}\n";
    }

    public function msgInfo(string $message): void
    {
        echo "{$message}\n";
    }
}