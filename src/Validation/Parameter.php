<?php

declare(strict_types=1);

namespace Boorwey\Console\Validation;

final class Parameter
{
    private string $string;

    private bool $isError;

    public function __construct(string $string)
    {
        $this->string = $string;
        $this->execute();
    }

    public function isError(): bool
    {
        return $this->isError;
    }

    private function execute(): void
    {
        $string = $this->string;
        $stringWithoutTags = substr($string, 1, -1);

        if (
            $string[0] === '['
            && substr($string, -1, 1) === ']'
            && !empty($stringWithoutTags)
        ) {
            $isError =
                strpos($stringWithoutTags, '[') !== false
                || strpos($stringWithoutTags, ']') !== false;

            if (!$isError) {
                $paramKeyVal = explode('=', $stringWithoutTags);

                if (
                    count($paramKeyVal) !== 2
                    || empty($paramKeyVal[0])
                    || empty($paramKeyVal[1])
                ) {
                    $isError = true;
                }
            }
        } else {
            $isError = true;
        }

        $this->isError = $isError ?? false;
    }
}