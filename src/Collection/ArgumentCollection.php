<?php

declare(strict_types=1);

namespace Boorwey\Console\Collection;

use Boorwey\Console\Model\ArgumentModel;

final class ArgumentCollection
{
    private array $items;

    public function add(ArgumentModel $model): void
    {
        $this->items[] = $model;
    }

    public function all(): array
    {
        return $this->items;
    }
}