<?php

declare(strict_types=1);

namespace Boorwey\Console\Collection;

use Boorwey\Console\Model\ParameterModel;

final class ParameterCollection
{
    private array $items;

    public function add(ParameterModel $model): void
    {
        $this->items[] = $model;
    }

    public function all(): array
    {
        return $this->items;
    }
}