<?php

declare(strict_types=1);

namespace Boorwey\Console\Command;

use Boorwey\Console\Cli\Output;
use Boorwey\Console\Collection\ArgumentCollection;
use Boorwey\Console\Collection\ParameterCollection;
use Boorwey\Console\CommandAbstract;

final class Demo1Command extends CommandAbstract
{
    public static function getName(): string
    {
        return 'demo-1';
    }

    public static function getDescription(): string
    {
        return 'Выводит неограниченное кол-во аргументов и параметров';
    }

    public static function getArguments(): ?ArgumentCollection
    {
        return null;
    }

    public static function isUnLimitArguments(): bool
    {
        return true;
    }

    public static function getParameters(): ?ParameterCollection
    {
        return null;
    }

    public static function isUnLimitParameters(): bool
    {
        return true;
    }

    public function execute(): void
    {
        $output = new Output();

        $output->msgInfo('Called command: ' . self::getName());

        $output->msgInfo('Arguments:');

        foreach ($this->input->arguments as $argument) {
            $output->msgInfo("\t- {$argument}");
        }

        $output->msgInfo('Parameters:');

        foreach ($this->input->parameters as $k => $parameter) {
            $output->msgInfo("- {$k}");
            $output->msgInfo("\t - {$parameter}");
        }
    }
}