<?php

declare(strict_types=1);

namespace Boorwey\Console\Command;

use Boorwey\Console\Cli\Output;
use Boorwey\Console\Collection\ArgumentCollection;
use Boorwey\Console\Collection\ParameterCollection;
use Boorwey\Console\CommandAbstract;
use Boorwey\Console\Model\ArgumentModel;
use Boorwey\Console\Model\ParameterModel;

final class Demo2Command extends CommandAbstract
{
    public static function getName(): string
    {
        return 'demo-2';
    }

    public static function getDescription(): string
    {
        return 'Выводит ограниченное кол-во аргументов и параметров';
    }

    public static function getArguments(): ?ArgumentCollection
    {
        $output = new ArgumentCollection();

        $output->add(new ArgumentModel('lastName', true));
        $output->add(new ArgumentModel('firstName', true));
        $output->add(new ArgumentModel('patronymic', false));

        return $output;
    }

    public static function isUnLimitArguments(): bool
    {
        return false;
    }

    public static function getParameters(): ?ParameterCollection
    {
        $output = new ParameterCollection();
        $output->add(new ParameterModel('age'));

        return $output;
    }

    public static function isUnLimitParameters(): bool
    {
        return false;
    }

    public function execute(): void
    {
        $input = $this->input;
        $output = new Output();

        $fullName = implode(' ', [
            $input->arguments['lastName'],
            $input->arguments['firstName'],
            $input->arguments['patronymic'],
        ]);
        $age = $input->parameters['age'];

        $output->msgInfo("Full name: {$fullName}");

        if (!empty($age)) {
            $output->msgInfo("Age: {$age}");
        }
    }
}