<?php

declare(strict_types=1);

namespace Boorwey\Console\Command;

use Boorwey\Console\CommandAbstract;

final class Map
{
    private array $items;

    public function __construct()
    {
        $items = [
            Demo1Command::getName() => Demo1Command::class,
            Demo2Command::getName() => Demo2Command::class,
        ];

        $folder = str_replace('/vendor/boorwey/console/src/Command', '', __DIR__);
        $folder .= '/console';

        $folderInside = scandir($folder);

        foreach ($folderInside as $item) {
            if (str_contains($item, 'Command')) {
                /** @var CommandAbstract $className */
                $className = '\Console\\' . basename($item, '.php');
                $items[$className::getName()] = $className;
            }
        }

        $this->items = $items;
    }

    public function getItems(): array
    {
        return $this->items;
    }
}