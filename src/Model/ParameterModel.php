<?php

declare(strict_types=1);

namespace Boorwey\Console\Model;

final class ParameterModel
{
    public readonly string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }
}