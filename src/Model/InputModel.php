<?php

declare(strict_types=1);

namespace Boorwey\Console\Model;

final class InputModel
{
    public readonly array $arguments;

    public readonly array $parameters;

    public function __construct(array $arguments, array $parameters)
    {
        $this->arguments = $arguments;
        $this->parameters = $parameters;
    }
}