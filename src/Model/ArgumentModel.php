<?php

declare(strict_types=1);

namespace Boorwey\Console\Model;

final class ArgumentModel
{
    public readonly string $name;

    public readonly bool $isRequired;

    public function __construct(string $name, bool $isRequired)
    {
        $this->name = $name;
        $this->isRequired = $isRequired;
    }
}