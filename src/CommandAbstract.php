<?php

declare(strict_types=1);

namespace Boorwey\Console;

use Boorwey\Console\Cli\Output;
use Boorwey\Console\Model\InputModel;
use Boorwey\Console\Model\ArgumentModel;
use Boorwey\Console\Model\ParameterModel;

use Boorwey\Console\Exception\UnLimitException;

use Boorwey\Console\Collection\ArgumentCollection;
use Boorwey\Console\Collection\ParameterCollection;

abstract class CommandAbstract
{
    protected InputModel $input;

    private array $errors = [];

    public function __construct(array $arguments, array $parameters)
    {
        self::isValidCommand();

        $this->fillInput($arguments, $parameters);

        if (!static::isUnLimitArguments()) {
            $this->validate();
        }
    }

    abstract public static function getName(): string;

    abstract public static function getDescription(): string;

    abstract public static function getArguments(): ?ArgumentCollection;

    abstract public static function isUnLimitArguments(): bool;

    abstract public static function getParameters(): ?ParameterCollection;

    abstract public static function isUnLimitParameters(): bool;

    abstract public function execute(): void;

    final public function getErrors(): array
    {
        return $this->errors;
    }

    private function fillInput(array $arguments, array $parameters): void
    {
        $this->input = new InputModel(
            $this->getArgumentsFromCli($arguments),
            $this->getParametersFromCli($parameters)
        );
    }

    private function getArgumentsFromCli(array $cliArguments): array
    {
        $output = [];
        $isUnLimitArguments = static::isUnLimitArguments();

        if (!$isUnLimitArguments) {
            $arguments = static::getArguments()?->all() ?? [];

            /** @var ArgumentModel $argument */
            foreach ($arguments as $k => $argument) {
                $output[$argument->name] = $cliArguments[$k] ?? null;
            }
        } else {
            $output = $cliArguments;
        }

        return $output;
    }

    private function getParametersFromCli(array $cliParameters): array
    {
        $output = [];
        $isUnLimitParameters = static::isUnLimitParameters();

        if (!$isUnLimitParameters) {
            $parameters = static::getParameters()?->all() ?? [];

            /** @var ParameterModel $parameter */
            foreach ($parameters as $parameter) {
                $output[$parameter->name] = $cliParameters[$parameter->name] ?? null;
            }
        } else {
            $output = $cliParameters;
        }

        return $output;
    }

    private function validate(): void
    {
        $errors = [];
        $arguments = $this->input->arguments;

        /** @var ArgumentModel $argumentModel */
        foreach (static::getArguments()?->all() ?? [] as $argumentModel) {
            if (
                empty($arguments[$argumentModel->name])
                && $argumentModel->isRequired
            ) {
                $errors[] = "Argument {$argumentModel->name} required";
            }
        }

        $this->errors = $errors;
    }

    private static function isValidCommand(): void
    {
        $isUnLimitArguments = static::isUnLimitArguments();
        $isUnLimitParameters = static::isUnLimitParameters();

        if (
            $isUnLimitArguments
            && static::getArguments() !== null
        ) {
            throw new UnLimitException();
        }

        if (
            $isUnLimitParameters
            && static::getParameters() !== null
        ) {
            throw new UnLimitException();
        }
    }

    final public static function forHelp(): void
    {
        self::isValidCommand();

        $output = new Output();
        $arguments = static::getArguments()?->all() ?? [];
        $parameters = static::getParameters()?->all() ?? [];

        if (!empty($arguments)) {
            $output->msgInfo('Arguments:');

            /** @var ArgumentModel $argument */
            foreach ($arguments as $argument) {
                $str = $argument->name;

                if ($argument->isRequired) {
                    $str.= "(required)";
                }

                $output->msgInfo("\t{$str}");
            }
        } else {
            $output->msgInfo('No arguments');
        }

        if (!empty($parameters)) {
            $output->msgInfo('Parameters:');

            /** @var ParameterModel $parameter */
            foreach ($parameters as $parameter) {
                $output->msgInfo("\t{$parameter->name}");
            }
        } else {
            $output->msgInfo('No parameters');
        }
    }
}